# frozen_string_literal: true

require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module VersionGitlabCom
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    # config.load_defaults 6.0

    # Custom directories with classes and modules you want to be autoloadable and eager loaded.
    config.eager_load_paths += %W[#{config.root}/lib]

    config.cache_store = :redis_store

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters.push(:password, :password_confirmation, :private_token)

    # Enable per-form CSRF tokens. Previous versions had false.
    config.action_controller.per_form_csrf_tokens = true

    # Enable origin-checking CSRF mitigation. Previous versions had false.
    config.action_controller.forgery_protection_origin_check = true

    # Require `belongs_to` associations by default. Previous versions had false.
    config.active_record.belongs_to_required_by_default = true

    config.generators.templates.push("#{config.root}/generator_templates")
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    # Loads custom configuration from application.yml
    config.application = config_for(:application)
  end
end
