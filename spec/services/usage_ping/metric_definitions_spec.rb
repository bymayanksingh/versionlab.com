# frozen_string_literal: true

require 'spec_helper'

describe UsagePing::MetricDefinitions do
  describe '#execute' do
    let(:metric_definitions_yaml) { File.read(Rails.root.join('spec/support/service_ping/metric_definitions.yml')) }

    subject { described_class.new.execute }

    it 'returns key paths from the metric definitions YAML' do
      stub_api_request

      expect(subject).to match_array(YAML.safe_load(metric_definitions_yaml))
    end

    it 'makes a request to GitLab API' do
      req_stub = stub_api_request

      subject

      expect(req_stub).to have_been_requested
    end
  end

  def stub_api_request
    stub_request(:get, "https://gitlab.com/api/v4/usage_data/metric_definitions")
      .with(
        headers: {
          'Accept' => 'application/yaml',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Content-Type' => 'application/json',
          'User-Agent' => 'Ruby'
        })
      .to_return(status: 200, body: metric_definitions_yaml, headers: {})
  end
end
