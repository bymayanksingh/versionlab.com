# frozen_string_literal: true

require 'spec_helper'

describe BuildConvIndexResponseService do
  let(:latest_ping) { build(:usage_data, uuid: uuid, recorded_at: recorded_at) }
  let(:uuid) { 'a' }
  let(:recorded_at) { 2.days.ago }

  context 'when uuid is missing' do
    it 'returns empty response' do
      latest_ping.uuid = nil

      response = subject.execute(latest_ping)

      expect(response).to eq({})
    end
  end

  context 'when there is not enough data for calculations' do
    it 'returns empty response' do
      allow_any_instance_of(CalculateConvIndexService)
        .to receive(:execute)
        .and_return([false, {}])

      expect(subject.execute(latest_ping)).to eq({ usage_data_id: latest_ping.id })
    end
  end

  it 'passes uuid and end date to perform calculations' do
    expect_any_instance_of(CalculateConvIndexService)
      .to receive(:execute)
      .with(uuid, recorded_at)
      .and_return([false, {}])

    subject.execute(latest_ping)
  end

  context 'when calculations can be performed' do
    before do
      results = {
        'issues' => 1.23,
        'notes' => 0.45,
        'milestones' => 10_000.43,
        'boards' => 0.16,
        'merge_requests' => 0.65,
        'ci_pipelines' => 9.47,
        'environments' => 0.28,
        'deployments' => 2.48,
        'projects_prometheus_active' => 0.34,
        'service_desk_issues' => 1.63
      }

      allow_any_instance_of(CalculateConvIndexService)
        .to receive(:execute)
        .and_return([true, results])
    end

    it 'returns instance scores' do
      response = subject.execute(latest_ping)

      expect(response['instance_issues']).to eq 1.23
    end

    it 'returns hard coded leader scores' do
      response = subject.execute(latest_ping)

      expect(response['leader_notes']).to eq 19.03
    end

    it 'returns percentage scores' do
      response = subject.execute(latest_ping)

      expect(response['percentage_notes']).to be_within(2.3).of(2.4)
    end

    it 'limits instance score when greater than leader score' do
      response = subject.execute(latest_ping)

      expect(response['instance_milestones']).to eq 0.50
    end

    it 'stores ConversationalDevelopmentIndex data' do
      expect { subject.execute(latest_ping) }.to change(ConversationalDevelopmentIndex, :count).from(0).to(1)
    end

    it 'associates ConversationalDevelopmentIndex data to the latest ping' do
      subject.execute(latest_ping)

      expect(ConversationalDevelopmentIndex.last.usage_data).to eq(latest_ping)
    end
  end
end
