# frozen_string_literal: true

require 'spec_helper'

describe VersionCheckCreatorService do
  describe '#execute' do
    let(:gitlab_info) { Base64.urlsafe_encode64({ version: '9.4.0', type: 'CE' }.to_json) }
    let(:url) { 'http://gitlab.example.com' }
    let(:expected_request_data) do
      { 'HTTP_REFERER' => url, 'HTTP_USER_AGENT' => 'agent' }
    end

    let(:request) do
      double(env: expected_request_data.merge({ 'OTHER' => 'not' }))
    end

    subject { described_class.new(gitlab_info, request) }

    shared_examples 'existing hostname' do
      it 'returns status: "success"' do
        expect(subject.execute[:status]).to eq('success')
      end

      it 'creates a new version_check record' do
        expect { subject.execute }.to change(VersionCheck, :count).from(1).to(2)

        version_check = VersionCheck.last
        expect(version_check.request_data).to eq(expected_request_data.to_json)
        expect(version_check.gitlab_version).to eq('9.4.0')
        expect(version_check.referer_url).to eq('http://gitlab.example.com')
      end

      it 'does not create a new host record' do
        expect { subject.execute }.not_to change(Host, :count)
      end
    end

    shared_examples 'new hostname' do |result_message|
      it "returns status: '#{result_message}'" do
        expect(subject.execute[:status]).to eq(result_message)
      end

      it 'creates a new version_check record' do
        expect { subject.execute }.to change(VersionCheck, :count).from(0).to(1)

        version_check = VersionCheck.last
        expect(version_check.request_data).to eq(expected_request_data.to_json)
        expect(version_check.gitlab_version).to eq('9.4.0')
        expect(version_check.referer_url).to eq('http://gitlab.example.com')
      end

      it 'creates a new host record' do
        expect { subject.execute }.to change(Host, :count).from(0).to(1)
      end
    end

    shared_examples 'critical vulnerability' do |result|
      it "returns critical_vulnerability: #{result}" do
        expect(subject.execute[:critical_vulnerability]).to eq(result)
      end
    end

    context 'when everything is ok' do
      let!(:version) { create(:version, version: '9.4.0') }

      context 'when hostname does not exist' do
        include_examples 'new hostname', 'success'
      end

      context 'when hostname exists' do
        let(:host) { create(:host, url: 'gitlab.example.com') }
        let!(:version_check) { create(:version_check, host: host) }

        context 'when there is no usage data for the host' do
          include_examples 'existing hostname'
        end

        context 'when there is a usage data record for the host' do
          before do
            create(:usage_data, host: host)
          end

          include_examples 'existing hostname'
        end
      end
    end

    context 'when hostname is not a valid url' do
      let(:url) { 'something' }

      subject { described_class.new(gitlab_info, request) }

      it 'returns status: "failed"' do
        expect(subject.execute[:status]).to eq('failed')
      end

      it 'does not create a new version_check record' do
        expect { subject.execute }.not_to change(VersionCheck, :count)
      end

      it 'does not create a new host record' do
        expect { subject.execute }.not_to change(Host, :count)
      end
    end

    context 'when the version is not up to date but not vulnerable' do
      before do
        create(:version, version: '10.8.9', vulnerability_type: :not_vulnerable)
      end

      include_examples 'new hostname', 'warning'
      include_examples 'critical vulnerability', false
    end

    context 'when the version has vulnerability_type of non_critical' do
      before do
        create(:version, version: '9.4.0', vulnerability_type: :non_critical)
      end

      include_examples 'new hostname', 'danger'
      include_examples 'critical vulnerability', false
    end

    context 'when the version has vulnerability_type of critical' do
      before do
        create(:version, version: '9.4.0', vulnerability_type: :critical)
      end

      include_examples 'new hostname', 'danger'
      include_examples 'critical vulnerability', true
    end

    context 'when the gitlabinfo is not a valid JSON' do
      subject { described_class.new(Base64.urlsafe_encode64('some string'), request) }

      it 'returns status: "failed"' do
        expect(subject.execute[:status]).to eq('failed')
      end

      it 'logs the error' do
        expect(Rails.logger).to receive(:warn)

        subject.execute
      end
    end

    describe 'current_host_stat' do
      let(:hostname) { URI.parse(url).host }

      context 'when a current_host_stat does exist' do
        it 'updates a host stat' do
          host_stat = create(:current_host_stat, url: hostname, version: '0.0.0')

          subject.execute

          expect(host_stat.reset.version).to eq '9.4.0'
        end
      end

      context 'when a current_host_stat does not exist yet' do
        it 'creates a new current_host_stat with version check' do
          expect { subject.execute }.to change(CurrentHostStat, :count)
          expect(CurrentHostStat.last).to have_attributes(url: hostname,
                                                          host_created_at: Host.last.created_at,
                                                          usage_data_recorded_at: nil,
                                                          active_users_count: nil,
                                                          historical_max_users_count: nil,
                                                          user_count: nil,
                                                          version: VersionCheck.last.gitlab_version,
                                                          version_check_created_at: VersionCheck.last.created_at)
        end

        context 'when no url is provided' do
          let(:url) {}

          it 'has no record created' do
            expect { subject.execute }.not_to change(CurrentHostStat, :count)
          end
        end

        it 'has no version check info' do
          subject = described_class.new(nil, request)

          expect { subject.execute }.not_to change(CurrentHostStat, :count)
        end
      end
    end
  end
end
