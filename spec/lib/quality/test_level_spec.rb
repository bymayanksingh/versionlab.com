# frozen_string_literal: true

require 'fast_spec_helper'

require_relative '../../../lib/quality/test_level'

RSpec.describe Quality::TestLevel do
  describe '#pattern' do
    context 'when level is unit' do
      it 'returns a pattern' do
        pattern = "spec/{bin,config,db,helpers,initializers,"\
                  "lib,models,routing,rubocop,services,tasks,tooling,views,workers}{,/**/}*_spec.rb"

        expect(subject.pattern(:unit))
          .to eq(pattern)
      end
    end

    context 'when level is migration' do
      it 'returns a pattern' do
        expect(subject.pattern(:migration))
          .to eq("spec/{migrations,lib/gitlab/background_migration}{,/**/}*_spec.rb")
      end
    end

    context 'when level is background_migration' do
      it 'returns a pattern' do
        expect(subject.pattern(:background_migration))
          .to eq("spec/{lib/gitlab/background_migration}{,/**/}*_spec.rb")
      end
    end

    context 'when level is integration' do
      it 'returns a pattern' do
        expect(subject.pattern(:integration))
          .to eq("spec/{controllers,mailers,requests}{,/**/}*_spec.rb")
      end
    end

    context 'when level is system' do
      it 'returns a pattern' do
        expect(subject.pattern(:system))
          .to eq("spec/{features}{,/**/}*_spec.rb")
      end
    end

    describe 'performance' do
      it 'memoizes the pattern for a given level' do
        expect(subject.pattern(:system).object_id).to eq(subject.pattern(:system).object_id)
      end

      it 'freezes the pattern for a given level' do
        expect(subject.pattern(:system)).to be_frozen
      end
    end
  end

  describe '#regexp' do
    context 'when level is unit' do
      it 'returns a regexp' do
        expect(subject.regexp(:unit))
          .to eq(%r{spec/(bin|config|db|helpers|initializers|lib|models|routing|rubocop|services|tasks|tooling|views|workers)})
      end
    end

    context 'when level is migration' do
      it 'returns a regexp' do
        expect(subject.regexp(:migration))
          .to eq(%r{spec/(migrations|lib/gitlab/background_migration)})
      end
    end

    context 'when level is background_migration' do
      it 'returns a regexp' do
        expect(subject.regexp(:background_migration))
          .to eq(%r{spec/(lib/gitlab/background_migration)})
      end
    end

    context 'when level is integration' do
      it 'returns a regexp' do
        expect(subject.regexp(:integration))
          .to eq(%r{spec/(controllers|mailers|requests)})
      end
    end

    context 'when level is system' do
      it 'returns a regexp' do
        expect(subject.regexp(:system))
          .to eq(%r{spec/(features)})
      end
    end

    describe 'performance' do
      it 'memoizes the regexp for a given level' do
        expect(subject.regexp(:system).object_id).to eq(subject.regexp(:system).object_id)
      end

      it 'freezes the regexp for a given level' do
        expect(subject.regexp(:system)).to be_frozen
      end
    end
  end

  describe '#level_for' do
    it 'returns the correct level for a unit test' do
      expect(subject.level_for('spec/models/abuse_report_spec.rb')).to eq(:unit)
    end

    it 'returns the correct level for a migration test' do
      expect(subject.level_for('spec/migrations/add_default_and_free_plans_spec.rb')).to eq(:migration)
    end

    it 'returns the correct level for a background migration test' do
      expect(subject.level_for('spec/lib/gitlab/background_migration/archive_legacy_traces_spec.rb')).to eq(:migration)
    end

    it 'returns the correct level for an integration test' do
      expect(subject.level_for('spec/mailers/abuse_report_mailer_spec.rb')).to eq(:integration)
    end

    it 'returns the correct level for a system test' do
      expect(subject.level_for('spec/features/abuse_report_spec.rb')).to eq(:system)
    end

    it 'raises an error for an unknown level' do
      expect { subject.level_for('spec/unknown/foo_spec.rb') }
        .to raise_error(described_class::UnknownTestLevelError,
                        %r{Test level for spec/unknown/foo_spec.rb couldn't be set})
    end
  end

  describe '#background_migration?' do
    it 'returns false for a unit test' do
      expect(subject.background_migration?('spec/models/abuse_report_spec.rb')).to be(false)
    end

    it 'returns true for a migration test' do
      expect(subject.background_migration?('spec/migrations/add_default_and_free_plans_spec.rb')).to be(false)
    end

    it 'returns true for a background migration test' do
      expect(subject.background_migration?('spec/lib/gitlab/background_migration/archive_legacy_traces_spec.rb'))
        .to be(true)
    end
  end
end
