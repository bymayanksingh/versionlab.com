# frozen_string_literal: true

require 'spec_helper'

describe Gitlab::BackgroundMigration::DeleteUsageData, :migration do
  let(:hosts) { table(:hosts) }
  let(:usage_data) { table(:usage_data) }
  let(:raw_usage_data) { table(:raw_usage_data) }
  let(:current_host_stats) { table(:current_host_stats) }
  let(:avg_cycle_analytics) { table(:avg_cycle_analytics) }
  let(:conversational_development_indices) { table(:conversational_development_indices) }

  let(:host_1) { hosts.create!(url: 'https://gitlab.example.com') }
  let(:host_2) { hosts.create!(url: 'https://test.com') }

  subject(:migration) { described_class.new }

  describe '#perform' do
    let!(:raw_usage_data_1) { raw_usage_data.create!(payload: {}, uuid: SecureRandom.uuid, recorded_at: Time.now) }
    let!(:usage_data_1) { usage_data.create!(host_id: host_1.id, raw_usage_data_id: raw_usage_data_1.id) }
    let!(:usage_data_2) { usage_data.create!(host_id: host_1.id) }
    let!(:conv_index) do
      conversational_development_indices.create!(
        leader_boards: 0.33,
        instance_boards: rand(0.0..0.33).round(2),
        leader_ci_pipelines: 17.84,
        instance_ci_pipelines: rand(0.0..17.84).round(2),
        leader_deployments: 10.24,
        instance_deployments: rand(0.0..10.24).round(2),
        leader_environments: 0.83,
        instance_environments: rand(0.0..0.83).round(2),
        leader_issues: 3.31,
        instance_issues: rand(0.0..3.31).round(2),
        leader_merge_requests: 3.77,
        instance_merge_requests: rand(0.0..3.77).round(2),
        leader_milestones: 0.5,
        instance_milestones: rand(0.0..0.5).round(2),
        leader_notes: 19.03,
        instance_notes: rand(0.0..19.03).round(2),
        leader_projects_prometheus_active: 0.12,
        instance_projects_prometheus_active: rand(0.0..0.12).round(2),
        leader_service_desk_issues: 3.3,
        instance_service_desk_issues: rand(0.0..3.3).round(2),
        usage_data_id: usage_data_1.id
      )
    end
    let!(:current_host_stat_1) do
      current_host_stats.create!(
        host_id: host_1.id,
        url: host_1.url,
        host_created_at: host_1.created_at,
        active_users_count: 1337,
        historical_max_users_count: 3137,
        user_count: 1111,
        license_md5: 'd41d8cd98f00b204e9800998ecf8427e',
        edition: 'CE'
      )
    end
    let!(:cycle_analytics) { avg_cycle_analytics.create!(usage_data_id: usage_data_2.id) }

    let!(:usage_data_3) { usage_data.create!(host_id: host_2.id) }

    it 'deletes UsageData records' do
      expect { migration.perform(['missing-domain.example.com', host_1.url]) }
        .to change(usage_data, :count).by(-2)
        .and change(raw_usage_data, :count).by(-1)
        .and change(conversational_development_indices, :count).by(-1)
        .and change(avg_cycle_analytics, :count).by(-1)
    end

    it 'does not delete usage data of other hosts' do
      migration.perform(['missing-domain.example.com', host_1.url])

      expect(usage_data.pluck(:id)).to match_array([usage_data_3.id])
    end

    it 'sets usage data related attributes in host stats to nil' do
      migration.perform(['missing-domain.example.com', host_1.url])

      expect(current_host_stat_1.reset).to have_attributes(
        usage_data_recorded_at: nil,
        active_users_count: nil,
        historical_max_users_count: nil,
        user_count: nil,
        license_md5: nil,
        edition: nil
      )
    end
  end
end
