# frozen_string_literal: true

require 'spec_helper'

describe Host do
  describe 'Associations' do
    it { is_expected.to have_one :current_host_stat }
  end
end
