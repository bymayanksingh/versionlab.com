# frozen_string_literal: true

require 'spec_helper'

describe VersionCheck do
  let(:valid_attributes) do
    {
      host: nil,
      request_data: '{"HTTP_REFERER": "https://gitlab.com/admin"}',
      referer_url: 'https://gitlab.com/help',
      gitlab_version: '7.7.1'
    }
  end

  def create_version_check(version:, type:)
    create(:version_check, gitlab_version: version).tap do |record|
      create(:version, version: record.gitlab_version, vulnerability_type: type)
    end
  end

  subject { build(:version_check, valid_attributes) }

  it { is_expected.to be_valid }

  context 'with constraints' do
    it 'violates host constraint' do
      expect { create(:version_check, host: nil) }
        .to raise_error(ActiveRecord::StatementInvalid).with_message(/PG::NotNullViolation/)
    end
  end

  describe '.ordered_by_id_with_limit' do
    it 'returns correct size and order' do
      create(:version_check)
      vc_2 = create(:version_check)
      vc_3 = create(:version_check)

      expect(described_class.ordered_by_id_with_limit(2)).to match([vc_3, vc_2])
    end
  end

  describe '.uniq_gitlab_versions' do
    it 'returns a unique list without blanks' do
      create(:version_check, :skip_validation, gitlab_version: '')
      create(:version_check, gitlab_version: '2.2.2')
      create(:version_check, gitlab_version: '2.2.2')
      create(:version_check, gitlab_version: '1.1.1')

      expect(described_class.uniq_gitlab_versions).to match(%w[2.2.2 1.1.1])
    end
  end

  describe '.recent_first' do
    it 'returns correct size and order' do
      vc_2 = create(:version_check)
      vc_3 = create(:version_check)

      expect(described_class.recent_first).to match([vc_3, vc_2])
    end
  end

  describe '.up_to_date?' do
    it 'returns true if version is most recent' do
      create(:version, version: '2.2.1')
      create(:version, version: '2.2.2')

      not_most_recent_version = create(:version_check, gitlab_version: '2.2.1')
      most_recent_version = create(:version_check, gitlab_version: '2.2.2')

      expect(not_most_recent_version.up_to_date?).to be(false)
      expect(most_recent_version.up_to_date?).to be(true)
    end
  end

  describe '#update_required?' do
    it 'returns true if version is marked with a vulnerable vulnerability_type' do
      non_vulnerable_version = create_version_check(version: '2.2.1', type: :not_vulnerable)
      non_critical_version = create_version_check(version: '2.2.2', type: :non_critical)
      critical_version = create_version_check(version: '2.2.3', type: :critical)

      expect(non_vulnerable_version).not_to be_update_required
      expect(non_critical_version).to be_update_required
      expect(critical_version).to be_update_required
    end
  end

  describe '#critical_vulnerability?' do
    it 'returns true if version is marked with a critical vulnerability' do
      non_vulnerable_version = create_version_check(version: '2.2.1', type: :not_vulnerable)
      non_critical_version = create_version_check(version: '2.2.2', type: :non_critical)
      critical_version = create_version_check(version: '2.2.3', type: :critical)

      expect(non_vulnerable_version).not_to be_critical_vulnerability
      expect(non_critical_version).not_to be_critical_vulnerability
      expect(critical_version).to be_critical_vulnerability
    end
  end
end
