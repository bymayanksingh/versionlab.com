# frozen_string_literal: true

module MigrationsHelpers
  def active_record_base
    ActiveRecord::Base
  end

  def table(name)
    Class.new(active_record_base) do
      self.table_name = name
      self.inheritance_column = :_type_disabled

      alias_method :reset, :reload

      def self.name
        table_name.singularize.camelcase
      end
    end
  end

  def migration_context
    ActiveRecord::MigrationContext.new(migrations_paths, ActiveRecord::SchemaMigration)
  end

  def migrate!
    migration_context.up do |migration|
      migration.name == described_class.name
    end
  end

  def migrations
    migration_context.migrations
  end

  def migrations_paths
    ActiveRecord::Migrator.migrations_paths
  end

  def schema_migrate_down!
    disable_migrations_output do
      migration_context.down(migration_schema_version)
    end

    reset_column_in_all_models
  end

  def schema_migrate_up!
    reset_column_in_all_models

    disable_migrations_output do
      migration_context.up
    end

    reset_column_in_all_models
  end

  def reset_column_in_all_models
    clear_schema_cache!

    # Reset column information for the most offending classes **after** we
    # migrated the schema up, otherwise, column information could be
    # outdated. We have a separate method for this so we can override it in EE.
    ActiveRecord::Base.descendants.each(&method(:reset_column_information))
  end

  def clear_schema_cache!
    ActiveRecord::Base.connection_pool.connections.each do |conn|
      conn.schema_cache.clear!
    end
  end

  def reset_column_information(klass)
    klass.reset_column_information
  end

  def disable_migrations_output
    ActiveRecord::Migration.verbose = false

    yield
  ensure
    ActiveRecord::Migration.verbose = true
  end

  def migration_schema_version
    metadata_schema = self.class.metadata[:schema]

    if metadata_schema == :latest
      migrations.last.version
    else
      metadata_schema || previous_migration.version
    end
  end

  def previous_migration
    migrations.each_cons(2) do |previous, migration|
      break previous if migration.name == described_class.name
    end
  end
end
