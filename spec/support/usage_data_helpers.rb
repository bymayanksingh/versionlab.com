# frozen_string_literal: true

module UsageDataHelpers
  DEFAULT_COUNTS = {
    auto_devops_disabled: 3,
    auto_devops_enabled: 8,
    boards: 0,
    ci_builds: 83,
    ci_external_pipelines: 5,
    ci_internal_pipelines: 8,
    ci_pipeline_config_auto_devops: 3,
    ci_pipeline_config_repository: 5,
    ci_pipeline_schedules: 14,
    ci_pipelines: 40,
    ci_runners: 0,
    ci_triggers: 0,
    deploy_keys: 0,
    deployments: 0,
    environments: 0,
    geo_nodes: 0,
    groups: 4,
    in_review_folder: 9,
    issues: 80,
    keys: 10,
    labels: 0,
    ldap_group_links: 0,
    ldap_keys: 0,
    ldap_users: 0,
    lfs_objects: 0,
    merge_requests: 42,
    milestones: 40,
    notes: 895,
    pages_domains: 0,
    projects_jira_active: 17,
    projects: 11,
    projects_imported_from_github: 3,
    projects_prometheus_active: 1,
    projects_slack_active: 1,
    projects_slack_slash_commands_active: 1,
    protected_branches: 16,
    releases: 0,
    remote_mirrors: 0,
    service_desk_enabled_projects: 1,
    service_desk_issues: 200,
    services: 0,
    snippets: 50,
    todos: 102,
    uploads: 702,
    web_hooks: 1
  }.freeze

  FEATURES_KEYS_REPLACED = [
    :container_registry,
    :elasticsearch,
    :geo,
    :gitlab_shared_runners,
    :gravatar,
    :ldap,
    :omniauth,
    :reply_by_email,
    :signup
  ].freeze

  def common_test_usage_data
    {
      uuid: '00000000-0000-0000-0000-000000000000',
      hostname: 'example.com',
      installation_type: 'source',
      active_user_count: 50,
      recorded_at: '2018-06-19T20:00:00.00-03:00',
      recording_ce_finished_at: '2018-06-19T20:01:00.00-03:00',
      recording_ee_finished_at: '2018-06-19T20:05:00.00-03:00',
      mattermost_enabled: false,
      edition: 'EEU',
      license_md5: '7de2a9aa2036e4919f3d1bfa479fef02',
      license_sha256: 'VXU9WgVzIGI9EcYVHS87B0ecp4IS9gFI54YcHxn8c2soGPMEKQqkaJLfEeHYA2Dj',
      license_id: 1,
      historical_max_users: 10,
      licensee: {
        'Name': 'GitLab, Inc.',
        'Company': 'GitLab, Inc.',
        'Email': 'test@gitlab.com'
      },
      license_user_count: 100,
      license_starts_at: '2017-11-20',
      license_expires_at: '2017-12-20',
      license_plan: 'ultimate',
      license_add_ons: {
        'GitLab_FileLocks': 1,
        'GitLab_Geo': 1
      },
      license_trial: false,
      reply_by_email: false,
      gitlab_pages: {
        enabled: false,
        version: '0.9.1'
      },
      git: {
        version: {
          major: 2,
          minor: 17,
          patch: 1
        }
      },
      gitaly: {
        servers: 3,
        filesystems: %w[ext4 NFS],
        version: "1.2.4",
        clusters: 1
      },
      container_registry_server: {
        vendor: 'gitlab',
        version: '2.9.1-gitlab'
      },
      database: {
        adapter: 'postgresql',
        version: '9.6.9',
        pg_system_id: 6842684531675334351,
        flavor: "Cloud SQL for PostgreSQL"
      },
      mail: {
        smtp_server: 'email.server.com'
      },
      app_server: {
        type: 'console'
      },
      object_store: {
        artifacts: {
          enabled: true,
          object_store: { enabled: true, direct_upload: true, background_upload: false, provider: "AWS" }
        },
        external_diffs: { enabled: false },
        lfs: {
          enabled: true,
          object_store: { enabled: false, direct_upload: true, background_upload: false, provider: "AWS" }
        },
        uploads: {
          enabled: nil,
          object_store: { enabled: false, direct_upload: true, background_upload: false, provider: "AWS" }
        },
        packages: {
          enabled: true,
          object_store: { enabled: false, direct_upload: false, background_upload: true, provider: "AWS" }
        }
      },
      topology: {
        nodes: [
          {
            node_total_memory: 10000000,
            node_free_memory: 5000000
          },
          {
            node_total_memory: 8000000
          }
        ]
      },
      analytics_unique_visits: {
        g_analytics_contribution: 999,
        g_analytics_insights: 999,
        g_analytics_issues: 999,
        g_analytics_productivity: 999,
        g_analytics_valuestream: 999,
        p_analytics_pipelines: 999,
        p_analytics_code_reviews: 999,
        p_analytics_valuestream: 999,
        p_analytics_insights: 999,
        p_analytics_issues: 999,
        p_analytics_repo: 999,
        u_analytics_todos: 999,
        i_analytics_cohorts: 999,
        i_analytics_dev_ops_score: 999,
        p_analytics_merge_request: 999,
        g_analytics_merge_request: 999

      },
      compliance_unique_visits: {
        g_compliance_dashboard: 999,
        g_compliance_audit_events: 999,
        i_compliance_audit_events: 999,
        i_compliance_credential_inventory: 999,
        compliance_unique_visits_for_any_target: 999,
        compliance_unique_visits_for_any_target_monthly: 999
      },
      search_unique_visits: {
        i_search_total: 999,
        i_search_advanced: 999,
        i_search_paid: 999
      },
      redis_hll_counters: {
        compliance: {
          g_compliance_dashboard: 999,
          g_compliance_audit_events: 999,
          i_compliance_audit_events: 999,
          i_compliance_credential_inventory: 999,
          compliance_total_unique_counts_weekly: 999,
          compliance_total_unique_counts_monthly: 999
        },
        analytics: {
          g_analytics_contribution: 999,
          g_analytics_insights: 999,
          g_analytics_issues: 999,
          g_analytics_productivity: 999,
          g_analytics_valuestream: 999,
          p_analytics_pipelines: 999,
          p_analytics_code_reviews: 999,
          p_analytics_valuestream: 999,
          p_analytics_insights: 999,
          p_analytics_issues: 999,
          p_analytics_repo: 999,
          i_analytics_cohorts: 999,
          i_analytics_dev_ops_score: 999,
          g_analytics_merge_request: 999,
          p_analytics_merge_request: 999,
          analytics_total_unique_counts_weekly: 999,
          analytics_total_unique_counts_monthly: 999
        },
        ide_edit: {
          g_edit_by_web_ide: 999,
          g_edit_by_sfe: 999,
          g_edit_by_snippet_ide: 999,
          ide_edit_total_unique_counts_weekly: 999,
          ide_edit_total_unique_counts_monthly: 999
        },
        search: {
          i_search_total: 999,
          i_search_advanced: 999,
          i_search_paid: 999,
          search_total_unique_counts_weekly: 999,
          search_total_unique_counts_monthly: 999
        }
      },
      settings: {
        snowplow_enabled: true
      },
      license: {},
      counts_weekly: {
        aggregated_metrics: {
          code_review_category_monthly_active_users: 123
        }
      }
    }
  end

  def test_recent_stats
    {
      assignee_lists: 0,
      boards: 0,
      ci_builds: 900,
      ci_internal_pipelines: 45,
      ci_external_pipelines: 0,
      ci_pipeline_config_auto_devops: 0,
      ci_pipeline_config_repository: 0,
      ci_runners: 0,
      ci_triggers: 0,
      ci_pipeline_schedules: 0,
      auto_devops_enabled: 0,
      auto_devops_disabled: 0,
      deploy_keys: 0,
      deployments: 135,
      successful_deployments: 45,
      failed_deployments: 0,
      environments: 10,
      clusters: 0,
      clusters_enabled: 0,
      project_clusters_enabled: 0,
      group_clusters_enabled: 0,
      clusters_disabled: 0,
      project_clusters_disabled: 0,
      group_clusters_disabled: 0,
      clusters_platforms_gke: 0,
      clusters_platforms_user: 0,
      clusters_applications_helm: 0,
      clusters_applications_ingress: 0,
      clusters_applications_cert_managers: 0,
      clusters_applications_prometheus: 0,
      clusters_applications_runner: 0,
      clusters_applications_knative: 0,
      in_review_folder: 0,
      groups: 8,
      issues: 80,
      keys: 10,
      label_lists: 0,
      lfs_objects: 0,
      milestone_lists: 0,
      milestones: 40,
      pages_domains: 0,
      pool_repositories: 0,
      projects: 10,
      projects_imported_from_github: 0,
      projects_with_repositories_enabled: 10,
      projects_with_error_tracking_enabled: 0,
      protected_branches: 0,
      releases: 0,
      remote_mirrors: 0,
      snippets: 50,
      suggestions: 0,
      todos: 99,
      uploads: 0,
      web_hooks: 0,
      projects_slack_active: 0,
      projects_slack_slash_commands_active: 0,
      projects_prometheus_active: 0,
      projects_jira_server_active: 0,
      projects_jira_cloud_active: 0,
      projects_jira_active: 0,
      projects_jira_dvcs_cloud_active: 0,
      projects_jira_dvcs_server_active: 0,
      labels: 109,
      merge_requests: 32,
      notes: 988,
      epics: 0,
      feature_flags: 0,
      geo_nodes: 0,
      incident_issues: 0,
      ldap_group_links: 0,
      ldap_keys: 0,
      ldap_users: 0,
      pod_logs_usages_total: 0,
      projects_enforcing_code_owner_approval: 0,
      projects_mirrored_with_pipelines_enabled: 0,
      projects_reporting_ci_cd_back_to_github: 0,
      projects_with_packages: 0,
      projects_with_prometheus_alerts: 0,
      sast_jobs: 45,
      dependency_scanning_jobs: 45,
      container_scanning_jobs: 45,
      dast_jobs: 45,
      epics_deepest_relationship_level: nil
    }
  end

  def test_usage_broken_stats
    {
      user_preferences: {
        group_overview_details: 26,
        group_overview_security_dashboard: 0
      },
      operations_dashboard: {
        default_dashboard: 0,
        users_with_projects_added: 0
      }
    }.merge test_recent_stats
  end

  def test_fixed_flattened_stats
    {
      user_preferences_group_overview_details: 0,
      user_preferences_group_overview_security_dashboard: 0,
      operations_dashboard_default_dashboard: 0,
      operations_dashboard_users_with_projects_added: 0,
      epics_deepest_relationship_level: 0
    }
  end

  USAGE_DATA_STATS_RECENT = {
    assignee_lists: 0,
    boards: 0,
    ci_builds: 900,
    ci_internal_pipelines: 45,
    ci_external_pipelines: 0,
    ci_pipeline_config_auto_devops: 0,
    ci_pipeline_config_repository: 0,
    ci_runners: 0,
    ci_triggers: 0,
    ci_pipeline_schedules: 0,
    auto_devops_enabled: 0,
    auto_devops_disabled: 0,
    deploy_keys: 0,
    deployments: 135,
    successful_deployments: 45,
    failed_deployments: 0,
    environments: 10,
    clusters: 0,
    clusters_enabled: 0,
    project_clusters_enabled: 0,
    group_clusters_enabled: 0,
    clusters_disabled: 0,
    project_clusters_disabled: 0,
    group_clusters_disabled: 0,
    clusters_platforms_gke: 0,
    clusters_platforms_user: 0,
    clusters_applications_helm: 0,
    clusters_applications_ingress: 0,
    clusters_applications_cert_managers: 0,
    clusters_applications_prometheus: 0,
    clusters_applications_runner: 0,
    clusters_applications_knative: 0,
    in_review_folder: 0,
    groups: 8,
    issues: 80,
    keys: 10,
    label_lists: 0,
    lfs_objects: 0,
    milestone_lists: 0,
    milestones: 40,
    pages_domains: 0,
    pool_repositories: 0,
    projects: 10,
    projects_imported_from_github: 0,
    projects_with_repositories_enabled: 10,
    projects_with_error_tracking_enabled: 0,
    protected_branches: 0,
    releases: 0,
    remote_mirrors: 0,
    snippets: 50,
    suggestions: 0,
    todos: 99,
    uploads: 0,
    web_hooks: 0,
    projects_slack_active: 0,
    projects_slack_slash_commands_active: 0,
    projects_prometheus_active: 0,
    projects_jira_server_active: 0,
    projects_jira_cloud_active: 0,
    projects_jira_active: 0,
    projects_jira_dvcs_cloud_active: 0,
    projects_jira_dvcs_server_active: 0,
    labels: 109,
    merge_requests: 32,
    notes: 988,
    epics: 0,
    feature_flags: 0,
    geo_nodes: 0,
    incident_issues: 0,
    ldap_group_links: 0,
    ldap_keys: 0,
    ldap_users: 0,
    pod_logs_usages_total: 0,
    projects_enforcing_code_owner_approval: 0,
    projects_mirrored_with_pipelines_enabled: 0,
    projects_reporting_ci_cd_back_to_github: 0,
    projects_with_packages: 0,
    projects_with_prometheus_alerts: 0,
    sast_jobs: 45,
    dependency_scanning_jobs: 45,
    container_scanning_jobs: 45,
    dast_jobs: 45,
    epics_deepest_relationship_level: nil
  }.freeze

  USAGE_BROKEN_STATS = {
    user_preferences: {
      group_overview_details: 26,
      group_overview_security_dashboard: 0
    },
    operations_dashboard: {
      default_dashboard: 0,
      users_with_projects_added: 0
    }
  }.merge USAGE_DATA_STATS_RECENT

  USAGE_GOOD_STATS = {
    user_preferences_group_overview_details: 26,
    user_preferences_group_overview_security_dashboard: 0,
    operations_dashboard_default_dashboard: 0,
    operations_dashboard_users_with_projects_added: 0
  }.merge USAGE_DATA_STATS_RECENT
            .merge(epics_deepest_relationship_level: 0)

  def test_usage_fixed_stats
    {
      user_preferences_group_overview_details: 26,
      user_preferences_group_overview_security_dashboard: 0,
      operations_dashboard_default_dashboard: 0,
      operations_dashboard_users_with_projects_added: 0
    }.merge test_recent_stats
  end

  def test_usage_fixed_epics_stats
    USAGE_GOOD_STATS
  end

  def test_usage_data_fixed_epics_stats
    {
      stats: test_usage_fixed_epics_stats
    }.merge common_test_usage_data_recent
  end

  def common_test_usage_data_recent
    {
      version: "12.3.0-pre",
      container_registry_enabled: false,
      grafana_link_enabled: false,
      gitpod_enabled: false,
      auto_devops_enabled: false,
      dependency_proxy_enabled: false,
      ingress_modsecurity_enabled: false,
      elasticsearch_enabled: false,
      geo_enabled: false,
      gitlab_shared_runners_enabled: true,
      gravatar_enabled: true,
      ldap_enabled: false,
      license_trial_ends_on: '2017-12-20',
      omniauth_enabled: true,
      prometheus_enabled: true,
      prometheus_metrics_enabled: true,
      reply_by_email_enabled: false,
      signup_enabled: true,
      license_restricted_user_count: 1,
      web_ide_clientside_preview_enabled: true,
      counts_monthly: {
        snippets: 7
      },
      avg_cycle_analytics: {
        issue: { average: nil, sd: 0, missing: 10 },
        plan: { average: nil, sd: 0, missing: 10 },
        code: { average: nil, sd: 0, missing: 10 },
        test: { average: nil, sd: 0, missing: 10 },
        review: { average: nil, sd: 0, missing: 10 },
        staging: { average: nil, sd: 0, missing: 10 },
        production: { average: nil, sd: 0, missing: 10 },
        total: 70
      },
      usage_activity_by_stage: {
        manage: {
          groups: 19,
          ldap_keys: 0,
          ldap_users: 0
        }
      },
      usage_activity_by_stage_monthly: {
        manage: {
          events: 10,
          groups: 10,
          ldap_keys: 0,
          ldap_users: 0
        }
      }
    }.merge common_test_usage_data
  end

  def test_usage_data_recent
    {
      stats: test_usage_broken_stats
    }.merge common_test_usage_data_recent
  end

  def test_usage_data_fixed_stats
    {
      counts: test_usage_fixed_stats
    }.merge common_test_usage_data_recent
  end

  def test_usage_data
    {
      version: '11.0.0',
      signup: true,
      ldap: false,
      gravatar: true,
      omniauth: false,
      reply_by_email: false,
      container_registry: false,
      grafana_link_enabled: false,
      gitpod_enabled: false,
      auto_devops_enabled: false,
      dependency_proxy_enabled: false,
      ingress_modsecurity_enabled: false,
      gitlab_shared_runners: true,
      elasticsearch: false,
      geo: true,
      stats: DEFAULT_COUNTS.dup
    }.merge common_test_usage_data_recent
  end

  def test_usage_data_without_stats
    {
      version: '8.10.13-ee',
      signup: true,
      ldap: false,
      gravatar: true,
      omniauth: false,
      reply_by_email: false,
      container_registry: false,
      gitlab_shared_runners: true,
      elasticsearch: false,
      geo: true
    }.merge common_test_usage_data
  end

  def test_cycle_analytics_data
    {
      issue: {
        average: 561600,
        sd: 0,
        missing: 3
      },
      plan: {
        average: 633600,
        sd: 35272,
        missing: 3
      },
      code: {
        average: 648002,
        sd: 0,
        missing: 3
      },
      test: {
        average: nil,
        sd: 0,
        missing: 9
      },
      review: {
        average: 1368002,
        sd: 7887,
        missing: 3
      },
      staging: {
        average: nil,
        sd: 0,
        missing: 9
      },
      production: {
        average: nil,
        sd: 0,
        missing: 9
      },
      total: 3254402
    }
  end

  def usage_data_attributes(stats)
    {
      source_ip: '127.0.0.1',
      version: '12.3.0-pre',
      active_user_count: 50,
      license_md5: '7de2a9aa2036e4919f3d1bfa479fef02',
      license_sha256: 'VXU9WgVzIGI9EcYVHS87B0ecp4IS9gFI54YcHxn8c2soGPMEKQqkaJLfEeHYA2Dj',
      source_license_id: 1,
      historical_max_users: 10,
      licensee: {
        'Name' => 'GitLab, Inc.',
        'Company' => 'GitLab, Inc.',
        'Email' => 'test@gitlab.com'
      },
      license_user_count: 100,
      license_starts_at: Time.zone.parse('2017-11-20'),
      license_expires_at: Time.zone.parse('2017-12-20'),
      license_add_ons: {
        'GitLab_FileLocks' => 1,
        'GitLab_Geo' => 1
      },
      license_restricted_user_count: 1,
      recorded_at: Time.zone.parse('2018-06-19T20:00:00.00-03:00'),
      recording_ce_finished_at: Time.zone.parse('2018-06-19T20:01:00.00-03:00'),
      recording_ee_finished_at: Time.zone.parse('2018-06-19T20:05:00.00-03:00'),
      stats: stats,
      uuid: '00000000-0000-0000-0000-000000000000',
      edition: 'EEU',
      hostname: 'example.com',
      license_trial: false,
      installation_type: 'source',
      license_plan: 'ultimate',
      container_registry_vendor: 'gitlab',
      container_registry_version: '2.9.1-gitlab',
      database_adapter: 'postgresql',
      database_version: '9.6.9',
      database_pg_system_id: 6842684531675334351,
      database_flavor: 'Cloud SQL for PostgreSQL',
      mail_smtp_server: 'email.server.com',
      gitaly_version: '1.2.4',
      gitaly_filesystems: %w[ext4 NFS],
      gitaly_servers: 3,
      gitaly_clusters: 1,
      git_version: '2.17.1',
      mattermost_enabled: false,
      gitlab_pages_enabled: false,
      gitlab_pages_version: '0.9.1',
      container_registry_enabled: false,
      grafana_link_enabled: false,
      gitpod_enabled: false,
      auto_devops_enabled: false,
      dependency_proxy_enabled: false,
      ingress_modsecurity_enabled: false,
      elasticsearch_enabled: false,
      geo_enabled: false,
      gitlab_shared_runners_enabled: true,
      gravatar_enabled: true,
      ldap_enabled: false,
      license_trial_ends_on: Date.parse('2017-12-20'),
      omniauth_enabled: true,
      prometheus_enabled: true,
      prometheus_metrics_enabled: true,
      reply_by_email_enabled: false,
      signup_enabled: true,
      web_ide_clientside_preview_enabled: true
    }
  end

  def cycle_analytics_attributes
    {
      issue_average: 561600,
      issue_sd: 0,
      issue_missing: 3,
      plan_average: 633600,
      plan_sd: 35272,
      plan_missing: 3,
      code_average: 648002,
      code_sd: 0,
      code_missing: 3,
      test_average: nil,
      test_sd: 0,
      test_missing: 9,
      review_average: 1368002,
      review_sd: 7887,
      review_missing: 3,
      staging_average: nil,
      staging_sd: 0,
      staging_missing: 9,
      production_average: nil,
      production_sd: 0,
      production_missing: 9,
      total: 3254402
    }
  end

  def latest_service_ping_data
    test_usage_data.merge!(
      {
        counts_weekly: {
          aggregated_metrics: {
            code_review_category_monthly_active_users: 123
          }
        },
        license_billable_users: 2,
        license_subscription_id: 123,
        counts: test_recent_stats,
        instance_auto_devops_enabled: false,
        user_cap_feature_enabled: false
      }
    )
  end
end
