# frozen_string_literal: true

def stub_license_show_request(license_id, result)
  url = "http://localhost-cdot/api/v1/licenses/#{license_id}.json"

  WebMock.stub_request(:get, url).to_return(
    status: 200,
    headers: { 'Content-Type' => 'application/json' },
    body: result.to_json
  )
end

def stub_metric_definitions_request
  metric_definitions_yaml = File.read(Rails.root.join('spec/support/service_ping/metric_definitions.yml'))

  WebMock.stub_request(:get, "https://gitlab.com/api/v4/usage_data/metric_definitions")
    .with(
      headers: {
        'Accept' => 'application/yaml',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'Content-Type' => 'application/json',
        'User-Agent' => 'Ruby'
      })
    .to_return(status: 200, body: metric_definitions_yaml, headers: {})
end
