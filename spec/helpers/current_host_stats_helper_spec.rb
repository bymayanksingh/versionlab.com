# frozen_string_literal: true

require 'spec_helper'

describe CurrentHostStatsHelper do
  describe '.filter_params' do
    let(:common_sort_args) do
      {
        sort: 'abc',
        direction: 'desc'
      }
    end

    before do
      allow(helper).to receive(:sort_column).and_return('abc')
      allow(helper).to receive(:sort_direction).and_return('desc')
    end

    it 'has a search to perform with sorting' do
      search_args = { search: 'gitlab', search_type: 'License MD5' }
      filter = common_sort_args.merge(search_args)
      allow(helper).to receive(:params).and_return(search_args)

      expect(helper.filter_params).to match(filter)
    end

    it 'has no search to perform with sorting' do
      search_args = { search: nil, search_type: nil }
      allow(helper).to receive(:params).and_return(search_args)

      expect(helper.filter_params).to match(common_sort_args)
    end
  end

  describe '.search_params' do
    it 'has a search to perform with sorting' do
      search_args = { search: 'gitlab', search_type: 'License MD5' }
      allow(helper).to receive(:params).and_return(search_args)

      expect(helper.search_params).to match(search_args)
    end

    it 'has no search to perform with sorting' do
      search_args = { search: nil, search_type: nil }
      allow(helper).to receive(:params).and_return(search_args)

      expect(helper.search_params).to be_empty
    end
  end

  describe '.applied_filters' do
    it 'has a filter to apply' do
      params = { starred: true, fortune_rank: nil }
      allow(helper).to receive(:params).and_return(params)

      expect(helper.applied_filters).to match({ starred: true })
    end

    it 'has no filter to apply' do
      params = { starred: nil, fortune_rank: nil }
      allow(helper).to receive(:params).and_return(params)

      expect(helper.applied_filters).to be_empty
    end
  end

  describe '.licensed_user_count' do
    subject(:licensed_user_count) { helper.licensed_user_count(current_host_stat) }

    let(:user_count) { 250 }
    let(:current_host_stat) { build(:current_host_stat, user_count: user_count, zuora_account_id: zuora_account_id) }

    context 'when host stat has no salesforce account id' do
      let(:zuora_account_id) { nil }

      context 'when user_count is nil' do
        let(:user_count) { nil }

        it 'displays "unknown"' do
          expect(licensed_user_count).to eq('unknown')
        end
      end

      context 'when user_count exists' do
        it 'displays the user_count value' do
          expect(licensed_user_count).to eq(user_count.to_s)
        end
      end
    end

    context 'when host stat has a salesforce account id' do
      let(:zuora_account_id) { 'ab12' }

      shared_examples 'customers application link' do
        it 'displays the link to customers application' do
          expect(licensed_user_count).to include('Link to admin this billing account')
          expect(licensed_user_count).to include("admin/billing_account?query=#{zuora_account_id}")
        end
      end

      context 'when user_count is nil' do
        let(:user_count) { nil }

        it 'displays "unknown"' do
          expect(licensed_user_count).to include('>unknown<')
        end

        include_examples 'customers application link'
      end

      context 'when user_count exists' do
        it 'displays the user_count value' do
          expect(licensed_user_count).to include(">#{user_count}<")
        end

        include_examples 'customers application link'
      end
    end
  end
end
