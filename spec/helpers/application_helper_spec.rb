# frozen_string_literal: true

require 'spec_helper'

describe ApplicationHelper do
  describe '.calculate_sorting' do
    let(:column_name) { 'abc_def' }

    context 'when in initial column state' do
      it 'has a default sorting order and titleization applied' do
        allow(helper).to receive(:sort_column)
        allow(helper).to receive(:sort_direction)

        expect(helper.calculate_sorting(column_name, nil, 'desc')).to match(["Abc Def", column_name, 'desc', nil])
      end
    end

    context 'when in a set direction already' do
      before do
        allow(helper).to receive(:sort_column).and_return(column_name)
      end

      it 'changes the sorting direction from asc to desc' do
        allow(helper).to receive(:sort_direction).and_return('asc')

        expect(helper.calculate_sorting(column_name, nil, anything))
          .to match(["Abc Def", column_name, 'desc', 'current asc'])
      end

      it 'changes the sorting direction from desc to asc' do
        allow(helper).to receive(:sort_direction).and_return('desc')

        expect(helper.calculate_sorting(column_name, nil, anything))
          .to match(["Abc Def", column_name, 'asc', 'current desc'])
      end
    end
  end

  describe '.nav_link' do
    let(:link_text) { 'versions' }
    let(:link_path) { '/versions' }

    context 'when on current page' do
      before do
        allow(helper).to receive(:current_page?).and_return(true)
      end

      it 'adds nav-item class and nav-link active class' do
        nav_link = helper.nav_link(link_text, link_path)

        expect(nav_link).to include('class="nav-item"', 'class="nav-link active"')
      end
    end

    context 'when not on current page' do
      before do
        allow(helper).to receive(:current_page?).and_return(false)
      end

      it 'adds nav-item class and does not add nav-link active class' do
        nav_link = helper.nav_link(link_text, link_path)

        expect(nav_link).to include('class="nav-item"', 'class="nav-link"')
        expect(nav_link).not_to include('class="nav-link active"')
      end
    end
  end
end
