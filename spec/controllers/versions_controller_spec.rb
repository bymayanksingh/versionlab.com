# frozen_string_literal: true

require 'spec_helper'

describe VersionsController do
  let(:user) { create(:user) }

  before do
    sign_in(user)
  end

  describe 'GET #index' do
    render_views

    it 'displays the highest version first in the table' do
      create(:version, version: '6.0.0')
      create(:version, version: '7.0.0')
      create(:version, version: '7.10.1')
      create(:version, version: '7.9.2')

      get :index

      expect(response.body).to have_xpath('//table/tbody/tr[1]/td', text: '7.10.1')
      expect(response.body).to have_xpath('//table/tbody/tr[2]/td', text: '7.9.2')
      expect(response.body).to have_xpath('//table/tbody/tr[3]/td', text: '7.0.0')
      expect(response.body).to have_xpath('//table/tbody/tr[4]/td', text: '6.0.0')
    end

    context 'when Version has details' do
      let!(:version) { create(:version, version: '6.0.0', details: 'Some details') }

      it 'displays details data' do
        get :index

        expect(response.body).to have_xpath('//button[contains(text(), "Details")]')
        expect(response.body).to have_xpath("//*[@id='collapseDetails_#{version.id}']/div[contains(text()," \
                                              "'#{version.details}')]")
      end
    end

    context 'when version has no details' do
      let!(:version) { create(:version, version: '6.0.0') }

      it 'does not displays details' do
        get :index

        expect(response.body).not_to have_xpath('//button[contains(text(), "Details")]')
      end
    end
  end

  describe 'GET #new' do
    render_views

    it 'allows pre-filling the version string from params' do
      get :new, params: { version: '10.6.1' }

      expect(response.body).to match('10.6.1')
    end
  end

  describe 'POST #create' do
    it 'rejects invalid version' do
      expect { create_version(version: '9.0') }.not_to change(Version, :count)
    end

    it 'creates a new version' do
      expect { create_version }.to change(Version, :count).by(1)
    end

    it 'allows a `security_release_type` parameter' do
      params = ActionController::Parameters.new(
        version: "9.0.0",
        security_release_type: "non_critical",
        details: "Some details"
      ).permit!

      expect(Version).to receive(:new)
        .with(params)
        .and_call_original

      create_version(security_release_type: "non_critical", details: "Some details")
    end

    it 'redirects to versions_path' do
      create_version

      expect(response).to redirect_to versions_path
    end

    def create_version(version: '9.0.0', security_release_type: nil, details: nil)
      post :create, params: {
        version: {
          version: version,
          security_release_type: security_release_type,
          details: details
        }
      }
    end
  end

  describe 'PUT #update' do
    let(:version) { create(:version) }

    it 'allows a `vulnerability_type` parameter' do
      update_version(
        id: version.id,
        vulnerability_type: "non_critical",
        details: "Some details")
    end

    it 'redirects to versions_path' do
      update_version(id: version.id)

      expect(response).to redirect_to versions_path
    end

    def update_version(id: nil, version: '9.0.0', vulnerability_type: nil, details: nil)
      put :update, params: {
        id: id,
        version: {
          version: version,
          vulnerability_type: vulnerability_type,
          details: details
        }
      }
    end
  end

  describe 'DELETE #destroy' do
    it 'redirects to versions_path' do
      version = create(:version)

      delete :destroy, params: { id: version.id }

      expect(response).to redirect_to versions_path
    end
  end
end
