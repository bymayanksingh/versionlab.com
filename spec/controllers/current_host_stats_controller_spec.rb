# frozen_string_literal: true

require 'spec_helper'

describe CurrentHostStatsController do
  let(:user) { create(:user) }

  before do
    sign_in(user)
  end

  describe 'GET #index' do
    it 'shows available hosts' do
      get :index

      expect(response).to have_http_status(200)
    end

    context 'when sorting' do
      before do
        allow(CurrentHostStat).to receive_message_chain(:order, :page, :per)
      end

      it 'processes valid sort direction' do
        get :index, params: { direction: 'asc' }

        expect(CurrentHostStat).to have_received(:order).with('current_host_stats.url asc NULLS LAST')
      end

      it 'handles invalid sort direction' do
        get :index, params: { direction: 'bogus_sort' }

        expect(response).to have_http_status(422)
      end

      it 'defaults to desc when sort direction not passed' do
        get :index, params: { sort: 'version' }

        expect(CurrentHostStat).to have_received(:order).with('current_host_stats.version desc NULLS LAST')
      end

      it 'processes valid sort column' do
        get :index, params: { sort: 'active_users_count' }

        expect(CurrentHostStat).to have_received(:order).with('current_host_stats.active_users_count desc NULLS LAST')
      end

      it 'handles invalid sort column' do
        get :index, params: { sort: 'bogus_sort' }

        expect(response).to have_http_status(422)
      end

      it 'defaults to url when sort column not passed' do
        get :index, params: { direction: 'desc' }

        expect(CurrentHostStat).to have_received(:order).with('current_host_stats.url desc NULLS LAST')
      end
    end

    context 'when validating rendering' do
      render_views

      it 'shows starred hosts' do
        get :index, params: { starred: true }

        expect(response).to have_http_status(200)
      end

      it 'shows fortune ranked hosts' do
        get :index, params: { fortune_rank: true }

        expect(response).to have_http_status(200)
      end

      it 'responds with a csv file' do
        get :index, params: { format: 'csv' }

        expect(response).to have_http_status(200)
        expect(response.content_type).to eq("text/csv")
      end
    end
  end
end
