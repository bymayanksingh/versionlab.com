# frozen_string_literal: true

require 'spec_helper'

describe 'usage_data/show.html.haml' do
  let(:stats) do
    {
      'projects_with_expiration_policy_enabled_with_keep_n_set_to_1' => 22
    }
  end

  let(:counts_monthly) do
    {
      deployments: 283
    }
  end

  let(:counts_weekly) do
    {
      deployments: 83
    }
  end

  let(:usage_activity_by_stage) do
    {
      manage: {
        groups: 19
      }
    }
  end

  let(:usage_activity_by_stage_monthly) do
    {
      manage: {
        events: 10
      }
    }
  end

  it 'shows metrics with digits' do
    @usage_data = create(:usage_data, :with_license_data, stats: stats)

    render

    expect(rendered).to have_link('projects_with_expiration_policy_enabled_with_keep_n_set_to_1')
  end

  it 'shows usage activity by stage raw data' do
    @usage_data = create :usage_data,
                         usage_activity_by_stage: usage_activity_by_stage

    render

    expect(rendered).to have_content('usage_activity_by_stage')
    expect(rendered).to have_content('"groups"=>19')
  end

  it 'shows usage activity by stage monthly raw data' do
    @usage_data = create :usage_data,
                         usage_activity_by_stage_monthly: usage_activity_by_stage_monthly

    render

    expect(rendered).to have_content('usage_activity_by_stage_monthly')
    expect(rendered).to have_content('"events"=>10')
  end

  it 'shows usage counts_monthly raw data' do
    @usage_data = create :usage_data,
                         counts_monthly: counts_monthly,
                         counts_weekly: counts_weekly

    render

    expect(rendered).to have_content('counts_monthly')
    expect(rendered).to have_content('"deployments"=>283')
    expect(rendered).to have_content('counts_weekly')
    expect(rendered).to have_content('"deployments"=>83')
  end
end
