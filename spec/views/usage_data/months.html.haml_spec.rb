# frozen_string_literal: true

require 'spec_helper'

describe 'usage_data/months.html.haml' do
  let(:month_counts) do
    {
      "2019-12" => { "CE" => 123, "EE" => 456 },
      "2019-11" => { "CE" => 789, "EE" => 987 }
    }
  end

  before do
    assign(:month_counts, month_counts)
  end

  it 'has columns for all the editions' do
    editions_regex = UsageData::EDITIONS.map { |ed| "\\s+#{ed}" }.join

    render

    expect(rendered).to have_content(/Month#{editions_regex}/)
  end

  it 'does not show editions that are not tracked yet' do
    render

    expect(rendered).not_to have_content(/EE Rogue/)
  end

  it 'has the tally for each edition' do
    render

    expect(rendered).to have_content(/123 0 0 456 0 0\s.*\s789 0 0 987 0 0/)
  end
end
