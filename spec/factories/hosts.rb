# frozen_string_literal: true

FactoryBot.define do
  factory :host do
    sequence(:url) { |n| "https://gitlab#{n}.example.com" }
  end
end
