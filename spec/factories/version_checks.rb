# frozen_string_literal: true

FactoryBot.define do
  factory :version_check do
    host
    gitlab_version { '9.0.0' }
    referer_url { 'http://localhost' }

    trait :skip_validation do
      to_create { |instance| instance.save(validate: false) }
    end
  end
end
