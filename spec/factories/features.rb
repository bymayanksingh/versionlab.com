# frozen_string_literal: true

FactoryBot.define do
  factory :feature do
    version
    title { "Key Feature" }
    description { "This is a really cool new feature for you" }
    applicable_roles { %w[admin developer] }
    read_more_url { "https://gitlab.com" }
  end
end
