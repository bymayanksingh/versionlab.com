# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "admin#{n}@example.com" }
    password { '12345678' }
    private_token { '_private_token_' }
  end
end
