#!/bin/bash

DB_CONTAINER_NAME=versions_app_db
REDIS_CONTAINER_NAME=versions_app_redis

start_db(){
  db_running_containers=$(docker ps -a -f name=$DB_CONTAINER_NAME --format "{{.ID}}")

  if [ ! -z "$db_running_containers"  ]; then
    echo "Database is already running"
  else
    docker run -d --name $DB_CONTAINER_NAME -p 5432:5432 -v pgdata:/var/lib/postgresql/data -e POSTGRES_USER=gitlab -e POSTGRES_PASSWORD=postgres postgres:11-alpine
  fi
}

start_redis(){
  redis_running_containers=$(docker ps -a -f name=$REDIS_CONTAINER_NAME --format "{{.ID}}")

  if [ ! -z "$redis_running_containers"  ]; then
    echo "Redis is already running"
  else
    docker run -d --name $REDIS_CONTAINER_NAME -p 6379:6379 -v redisdata:/data redis:alpine
  fi
}

up() {
  start_db
  start_redis
}

down() {
  docker rm -f $DB_CONTAINER_NAME
  docker rm -f $REDIS_CONTAINER_NAME
}

if [[ $1 != "up" && $1 != "down" ]]; then
  echo "Unknown argument select 'up' or 'down'"
  exit 1
fi

if [[ $1 == "up" ]]; then
  up
else
  down
fi
