# frozen_string_literal: true

module API
  # Versions API
  class Versions < Grape::API
    resource :versions do
      # Get a versions list
      #
      # Example Request:
      #   GET /versions
      get do
        versions = Version.most_recent
        versions = paginate versions
        present versions, with: Entities::Version
      end

      # Get a single version
      #
      # Parameters:
      #   id or version (required) - The ID or version number (X.X.X)
      # Example Request:
      #   GET /versions/:id
      get ':id', requirements: { id: /v?[\d.]*/i } do
        version = Version.resolve!(params[:id])
        present version, with: Entities::Version
      end

      # Create a version
      #
      # Parameters:
      #   version (required) - The version number (X.X.X)
      #   vulnerability_type (optional) - ['not_vulnerable', 'non_critical', 'critical']
      #   security_release_type (optional) - ['not_vulnerable', 'non_critical', 'critical']
      #     When marked as 'non_critical' or 'critical' will execute logic to mark previous versions
      #     with the correct vulnerability_type.
      #   details - text describing version
      # Example Request:
      #   POST /versions
      post do
        authenticate!
        required_attributes! [:version]

        attrs = attributes_for_keys [:version, :vulnerability_type, :security_release_type, :details]
        version = Version.new(attrs)

        if version.save
          present version, with: Entities::Version
        else
          render_validation_error!(version)
        end
      end

      # Updates an existing version
      #
      # Parameters:
      #   id or version (required) - The ID or version number (X.X.X)
      #   vulnerability_type (optional) - ['not_vulnerable', 'non_critical', 'critical']
      #   security_release_type (optional) - ['not_vulnerable', 'non_critical', 'critical']
      #     When marked as 'non_critical' or 'critical' will execute logic to mark previous versions
      #     with the correct vulnerability_type.
      #   details - text describing version
      # Example Request:
      #   PUT /versions/:id
      put ':id', requirements: { id: /v?[\d.]*/i } do
        authenticate!

        attrs = attributes_for_keys [:vulnerability_type, :security_release_type, :details]
        version = Version.resolve!(params[:id])

        if version.update(attrs)
          present version, with: Entities::Version
        else
          render_validation_error!(version)
        end
      end

      # Remove a version
      #
      # Parameters:
      #   id or version (required) - The ID or version number (X.X.X)
      # Example Request:
      #   DELETE /versions/:id
      delete ':id', requirements: { id: /v?[\d.]*/i } do
        authenticate!

        version = Version.resolve!(params[:id])
        version.destroy
      end

      # Nested features
      #
      # Parameters:
      #   version id or version (required) - The ID or version number (X.X.X)
      # Example Request(s):
      #   GET /versions/:id/features....
      #   GET /versions/:id/features/:id
      segment '/:version_id', requirements: { version_id: /v?[\d.]*/i } do
        resource :features do
          # Get a features list for a version
          #
          # Example Request:
          #   GET /versions/:version_id/features
          get do
            version = Version.resolve!(params[:version_id])
            features = version.features
            present features, with: Entities::Feature
          end
        end
      end
    end
  end
end
