# frozen_string_literal: true

module Salesforce
  class AccountMatcherService < BaseService
    attr_reader :account_id

    def initialize(usage_data)
      @usage_data = usage_data
    end

    def execute
      return if usage_data.license_md5.blank? && usage_data.license_sha256.blank?

      license = LicenseAppService.new(
        license_sha256: usage_data.license_sha256,
        license_md5: usage_data.license_md5,
        license_id: usage_data.source_license_id
      ).get_license

      return unless license

      @zuora_subscription_id = license['zuora_subscription_id']
      @account_id = find_account_id_by_subscription_id

      unless account_id
        log("No account found")
        return
      end

      { account_id: account_id, zuora_subscription_id: zuora_subscription_id }
    end

    private

    attr_reader :usage_data,
                :zuora_subscription_id

    def cache_key
      "#{self.class.name}:zuora_subscription_id:#{zuora_subscription_id}"
    end

    def find_account_id_by_subscription_id
      return unless zuora_subscription_id.present?

      Rails.cache.fetch(cache_key, expires_in: 1.day) do
        soql = <<-QUERY.squish
          SELECT Zuora__Account__c
          FROM Zuora__Subscription__c
          WHERE Zuora__Zuora_Id__c = #{quote(zuora_subscription_id)}
        QUERY

        result = query(soql)
        count = result.size

        return if count.zero? # rubocop:disable Cop/AvoidReturnFromBlocks

        log("#{count} accounts found") if count > 1

        result.first[:Zuora__Account__c]
      end
    end

    def log(message)
      Rails.logger.warn("#{self.class.name}: #{message} for zuora_subscription_id #{zuora_subscription_id}")
    end
  end
end
