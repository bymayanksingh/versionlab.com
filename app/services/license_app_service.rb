# frozen_string_literal: true

class LicenseAppService
  def initialize(license_sha256:, license_md5:, license_id:)
    @license_sha256 = license_sha256
    @license_md5 = license_md5
    @license_id = license_id
  end

  def get_license
    return if !license_md5 && !license_sha256

    Rails.cache.fetch(cache_key, expires_in: 1.day) do
      find_license_by_id || find_license_by_md5 || find_license_by_sha256
    end
  end

  private

  attr_reader :license_sha256, :license_md5, :license_id

  def cache_key
    # usage data sent from older GitLab instances has empty sha256
    # usage data sent from FIPS enabled instances has empty md5
    "#{self.class.name}:#{license_sha256}:#{license_md5}"
  end

  def find_license_by_sha256
    return unless license_sha256

    result = make_license_request(license_show_url(license_sha256))

    result.to_h if result.present?
  end

  def find_license_by_id
    return unless license_id

    result = make_license_request(license_show_url(license_id))

    result.to_h if result.present?
  end

  def find_license_by_md5
    return unless license_md5

    result = make_license_request(license_show_url(license_md5))

    if result.nil?
      log("No license found")
      return nil
    end

    log("#{result.size} licenses found")

    result.to_h
  end

  def make_license_request(url)
    headers = {
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Admin-Email' => Rails.application.secrets.license_app_user_email.to_s,
      'X-Admin-Token' => Rails.application.secrets.license_app_user_token.to_s
    }

    HTTParty.get(url, headers: headers).parsed_response # rubocop:disable Gitlab/HTTParty
  rescue HTTParty::Error => e
    Rails.logger.info "Unable to contact GitLab License App: #{e}"
    nil
  end

  def license_show_url(identifier)
    "#{Rails.configuration.application.license_app_url}/api/v1/licenses/#{identifier}.json"
  end

  def log(message)
    Rails.logger.warn("#{self.class.name}: #{message} for license #{license_md5}")
  end
end
