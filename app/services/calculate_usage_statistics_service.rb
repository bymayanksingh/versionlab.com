# frozen_string_literal: true

class CalculateUsageStatisticsService
  def execute
    results = ActiveRecord::Base.connection.exec_query(
      <<-SQL.strip_heredoc
        SELECT key, AVG(value::bigint) as average
        FROM (
          SELECT
            DISTINCT on (uuid)
            stats
          FROM usage_data
          WHERE recorded_at > '#{30.days.ago}'
          ORDER BY uuid, recorded_at DESC
        ) as usage, json_each_text(stats)
        GROUP BY key
        ORDER BY key
      SQL
    )

    results.map do |row|
      [row['key'], Float(row['average'])]
    end.to_h
  end
end
