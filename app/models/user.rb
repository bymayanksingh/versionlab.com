# frozen_string_literal: true

class User < ApplicationRecord
  devise :database_authenticatable, :rememberable,
         :trackable, :validatable, :timeoutable

  devise :omniauthable, omniauth_providers: [:gitlab]

  before_save :ensure_private_token

  def self.from_omniauth(params)
    user = User.where(uid: params[:uid], provider: params[:provider]).last
    return user if user

    user = User.new
    user.email = params["info"]["email"]
    user.provider = params["provider"]
    user.uid = params["uid"]
    user.password = SecureRandom.hex
    user.save
    user
  end

  def reset_private_token!
    self.private_token = generate_private_token
    save!
  end

  private

  def ensure_private_token
    self.private_token ||= generate_private_token
  end

  def generate_private_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(private_token: token).exists?
    end
  end
end
