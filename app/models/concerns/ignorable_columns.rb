# frozen_string_literal: true

module IgnorableColumns
  extend ActiveSupport::Concern

  ColumnIgnore = Struct.new(:remove_after) do
    def safe_to_remove?
      Date.today > remove_after
    end

    def to_s
      "(#{remove_after})"
    end
  end

  class_methods do
    # Ignore database columns in a model
    #
    # Indicate the earliest date and release we can stop ignoring the column with +remove_after+ (a date string)
    def ignore_columns(*columns, remove_after:)
      msg = 'Please indicate when we can stop ignoring columns' \
            ' with remove_after (date string YYYY-MM-DD),' \
            ' example: ignore_columns(:name, remove_after: \'2019-12-01\')'
      raise ArgumentError, msg unless remove_after&.match?(utc_date_regex)

      self.ignored_columns += columns.flatten

      columns.flatten.each do |column|
        ignored_columns_details[column.to_sym] = ColumnIgnore.new(Date.parse(remove_after))
      end
    end

    alias_method :ignore_column, :ignore_columns

    def ignored_columns_details
      unless defined?(@ignored_columns_details)
        IGNORE_COLUMN_MUTEX.synchronize do
          @ignored_columns_details ||= superclass.try(:ignored_columns_details)&.dup || {}
        end
      end

      @ignored_columns_details
    end

    def utc_date_regex
      @utc_date_regex ||= /\A[0-9]{4}-[0-9]{2}-[0-9]{2}\z/.freeze
    end

    IGNORE_COLUMN_MUTEX = Mutex.new
  end
end
