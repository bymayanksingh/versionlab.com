# frozen_string_literal: true

class VersionCheck < ApplicationRecord
  belongs_to :host, optional: true, touch: true

  validates :gitlab_version, presence: true

  scope :with_host, -> { includes(:host) }
  scope :ordered_by_id_with_limit, ->(rows) { recent_first.limit(rows) }
  scope :uniq_gitlab_versions, -> { pluck(:gitlab_version).uniq.reject(&:blank?) }
  scope :recent_first, -> { order(id: :desc) }

  def self.search(term)
    where('LOWER(referer_url) LIKE ?', "%#{sanitize_sql_like(term.downcase)}%") # rubocop:disable GitlabSecurity/SqlInjection
  end

  def up_to_date?
    Version.latest?(general_gitlab_version)
  end

  def update_required?
    Version.where(version: general_gitlab_version)
      .where.not(vulnerability_type: Version.vulnerability_types[:not_vulnerable]).any?
  end

  def critical_vulnerability?
    Version.where(version: general_gitlab_version)
      .where(vulnerability_type: Version.vulnerability_types[:critical]).any?
  end

  def general_gitlab_version
    gitlab_version.sub(/-ee\Z/, '')
  end
end
