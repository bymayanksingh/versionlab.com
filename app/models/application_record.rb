# frozen_string_literal: true

class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  alias_method :reset, :reload

  def self.safe_ensure_unique(retries: 0)
    transaction(requires_new: true) do
      yield
    end
  rescue ActiveRecord::RecordNotUnique
    if retries.positive?
      retries -= 1
      retry
    end

    false
  end

  def self.safe_find_or_create_by!(*args)
    safe_ensure_unique(retries: 1) do
      find_or_initialize_by(*args).tap do |record|
        yield(record)
        record.save!
      end
    end
  end

  def self.safe_find_or_create_by(*args)
    safe_ensure_unique(retries: 1) do
      find_or_initialize_by(*args).tap do |record|
        yield(record)
        record.save
      end
    end
  end
end
