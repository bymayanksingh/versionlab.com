# frozen_string_literal: true

module UsageDataHelper
  def license_trial_description(trial)
    case trial
    when true
      'Yes'
    when false
      'No'
    when nil
      'Unknown'
    end
  end

  def license_usage_class(record)
    record.over? ? 'license-over' : 'license-under'
  end

  def license_usage_users(record)
    usage_users = record.usage_users
    return usage_users unless record.over?

    "+#{record.usage_users}"
  end
end
