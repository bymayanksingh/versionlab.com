# frozen_string_literal: true

class UsageDataWorker
  include ApplicationWorker

  def perform(usage_data_id)
    @usage_data = UsageData.find(usage_data_id)
    return unless usage_data.ee?

    @matcher = Salesforce::AccountMatcherService.new(usage_data)
    create_params = matcher.execute
    return unless create_params

    record_zuora_account

    Salesforce::PushUsageDataService.new(usage_data, create_params).execute
  end

  private

  attr_reader :usage_data,
              :matcher

  def record_zuora_account
    usage_data.host&.current_host_stat&.update(zuora_account_id: matcher.account_id)
  end
end
