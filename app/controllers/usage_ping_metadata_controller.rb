# frozen_string_literal: true

class UsagePingMetadataController < ApplicationController
  skip_before_action :verify_authenticity_token, only: :create
  skip_before_action :authenticate_user!, only: :create

  def create
    metadata = UsagePingMetadatum.new(metric_timing_params)

    if metadata.save
      head :created
    else
      Rails.logger.warn("Failed to save UsagePingMetadatum. Uuid: #{metric_timing_params[:uuid]}. " \
                          "Errors: #{metadata.errors.full_messages.join(', ')}")
      head :unprocessable_entity
    end
  end

  private

  def metric_timing_params
    params.require(:metadata).permit(:uuid, metrics: [:name, :time_elapsed, :error]).to_h
  end
end
