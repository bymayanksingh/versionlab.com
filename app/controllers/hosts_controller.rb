# frozen_string_literal: true

class HostsController < ApplicationController
  before_action :set_host

  def show
    @versions = VersionSorter.rsort(@host.version_checks.uniq_gitlab_versions)
    @checks = @host.version_checks.ordered_by_id_with_limit(20)
    @usage_ping_data = @host.usage_datum.ordered_by_id_with_limit(20)
  end

  def update
    @host.update(host_params)

    head :ok
  end

  private

  def set_host
    @host = Host.find(params[:id])
  end

  def host_params
    @host_params ||= params[:host].permit(:star)
  end
end
