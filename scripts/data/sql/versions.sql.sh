#!/bin/bash

export query="SELECT id
,created_at
,updated_at
,version
,vulnerability_type
,details
FROM versions
WHERE updated_at::date = '$date'"
