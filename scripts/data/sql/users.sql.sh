#!/bin/bash

export query="SELECT id
,created_at
,current_sign_in_at
,current_sign_in_ip
,email
,failed_attempts
,last_sign_in_at
,last_sign_in_ip
,locked_at
,provider
,sign_in_count
,uid
,updated_at
FROM users
WHERE updated_at::date = '$date'"
