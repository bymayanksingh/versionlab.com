# frozen_string_literal: true

class RemoveOldCountsFromUsageData < ActiveRecord::Migration[5.2]
  def change
    remove_column :usage_data, :old_counts, :text
  end
end
