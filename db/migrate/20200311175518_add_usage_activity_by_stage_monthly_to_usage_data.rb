# frozen_string_literal: true

class AddUsageActivityByStageMonthlyToUsageData < ActiveRecord::Migration[5.2]
  include Gitlab::Database::MigrationHelpers

  disable_ddl_transaction!

  def up
    add_column(:usage_data, :usage_activity_by_stage_monthly, :json, default: {}, null: true)
  end

  def down
    remove_column(:usage_data, :usage_activity_by_stage_monthly)
  end
end
