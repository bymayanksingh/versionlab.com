# frozen_string_literal: true

class AddHostIdToUsageData < ActiveRecord::Migration[4.2]
  def change
    add_column :usage_data, :host_id, :integer
  end
end
