# frozen_string_literal: true

class RemoveSmauFromUsageData < ActiveRecord::Migration[5.2]
  def change
    remove_column :usage_data, :smau, :json, default: {}
  end
end
