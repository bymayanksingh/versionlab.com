# frozen_string_literal: true

class AddIndexCreatedAtToRawUsageData < ActiveRecord::Migration[5.2]
  include Gitlab::Database::MigrationHelpers

  disable_ddl_transaction!

  def up
    add_concurrent_index :raw_usage_data, :created_at
  end

  def down
    remove_concurrent_index :raw_usage_data, :created_at
  end
end
