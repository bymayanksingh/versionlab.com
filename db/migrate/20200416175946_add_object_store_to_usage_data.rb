# frozen_string_literal: true

class AddObjectStoreToUsageData < ActiveRecord::Migration[5.2]
  def up
    add_column(:usage_data, :object_store, :json, null: true, default: {})
  end

  def down
    remove_column(:usage_data, :object_store)
  end
end
