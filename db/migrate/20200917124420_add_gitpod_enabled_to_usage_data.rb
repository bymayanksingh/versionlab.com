# frozen_string_literal: true

class AddGitpodEnabledToUsageData < ActiveRecord::Migration[5.2]
  def change
    add_column :usage_data, :gitpod_enabled, :boolean
  end
end
