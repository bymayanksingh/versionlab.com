# frozen_string_literal: true

class AddInSalesforceToHost < ActiveRecord::Migration[4.2]
  def change
    add_column :hosts, :in_salesforce, :boolean
  end
end
