# frozen_string_literal: true

class AddCountsMonthlyToUsageData < ActiveRecord::Migration[5.2]
  def change
    add_column :usage_data, :counts_monthly, :jsonb, default: {}
  end
end
