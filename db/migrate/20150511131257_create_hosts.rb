# frozen_string_literal: true

class CreateHosts < ActiveRecord::Migration[4.2]
  def change
    create_table :hosts do |t|
      t.string :url # rubocop:disable Migration/AddLimitToStringColumns

      t.timestamps null: false # rubocop:disable Migration/Timestamps
    end
  end
end
