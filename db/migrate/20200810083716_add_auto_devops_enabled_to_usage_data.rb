# frozen_string_literal: true

class AddAutoDevopsEnabledToUsageData < ActiveRecord::Migration[5.2]
  def change
    add_column :usage_data, :auto_devops_enabled, :boolean
  end
end
