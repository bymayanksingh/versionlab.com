# frozen_string_literal: true

class AddFuncIndexForBackfillToUsageData < ActiveRecord::Migration[5.2]
  include Gitlab::Database::MigrationHelpers

  INDEX_NAME = 'recorded_at_and_edit_by_function'

  disable_ddl_transaction!

  def up
    add_concurrent_index :usage_data, "date_trunc('month', recorded_at), COALESCE(edition, 'EE')", name: INDEX_NAME # rubocop:disable Migration/UpdateLargeTable

    remove_concurrent_index :usage_data, [:recorded_at, :edition]
  end

  def down
    add_concurrent_index :usage_data, [:recorded_at, :edition] # rubocop:disable Migration/UpdateLargeTable

    remove_concurrent_index_by_name :usage_data, INDEX_NAME
  end
end
