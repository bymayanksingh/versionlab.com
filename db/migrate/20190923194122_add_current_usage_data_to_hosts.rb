# frozen_string_literal: true

class AddCurrentUsageDataToHosts < ActiveRecord::Migration[5.0]
  def change
    add_reference :hosts, :current_usage_data, index: true, foreign_key: { on_delete: :cascade, to_table: :usage_data } # rubocop:disable Migration/AddReference
  end
end
