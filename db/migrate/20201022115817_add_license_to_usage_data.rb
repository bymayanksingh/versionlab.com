# frozen_string_literal: true

class AddLicenseToUsageData < ActiveRecord::Migration[5.2]
  def change
    add_column :usage_data, :license, :jsonb, default: {}
  end
end
