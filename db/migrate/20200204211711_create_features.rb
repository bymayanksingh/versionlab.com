# frozen_string_literal: true

class CreateFeatures < ActiveRecord::Migration[5.1]
  def change
    create_table :features do |t|
      t.references :version, foreign_key: { on_delete: :cascade }, null: false
      t.string :title, null: false # rubocop:disable Migration/AddLimitToStringColumns
      t.text :description, null: false
      t.string :applicable_roles, array: true, null: false, default: [] # rubocop:disable Migration/AddLimitToStringColumns
      t.string :read_more_url # rubocop:disable Migration/AddLimitToStringColumns

      t.timestamps # rubocop:disable Migration/Timestamps
    end
  end
end
