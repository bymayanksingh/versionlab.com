# frozen_string_literal: true

class AddSettingsToUsageData < ActiveRecord::Migration[5.2]
  def change
    add_column :usage_data, :settings, :jsonb, default: {}
  end
end
