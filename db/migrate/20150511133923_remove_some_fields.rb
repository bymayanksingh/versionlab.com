# frozen_string_literal: true

class RemoveSomeFields < ActiveRecord::Migration[4.2]
  def change
    # rubocop:disable Rails/ReversibleMigration
    remove_column :version_checks, :request_ip # rubocop:disable Rails/BulkChangeTable
    remove_column :version_checks, :gitlab_url
    # rubocop:enable Rails/ReversibleMigration
  end
end
