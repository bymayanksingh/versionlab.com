# frozen_string_literal: true

class CreateUsageData < ActiveRecord::Migration[4.2]
  def change
    # rubocop:disable Migration/AddLimitToStringColumns
    create_table :usage_data do |t|
      t.string :source_ip
      t.string :version
      t.integer :active_user_count
      t.string :license_md5
      t.integer :historical_max_users
      t.string :licensee
      t.integer :license_user_count
      t.datetime :license_starts_at
      t.datetime :license_expires_at
      t.string :license_add_ons
      t.integer :license_restricted_user_count
      t.string :license_add_ons
      t.datetime :recorded_at

      t.timestamps null: false # rubocop:disable Migration/Timestamps
    end
    # rubocop:enable Migration/AddLimitToStringColumns
  end
end
