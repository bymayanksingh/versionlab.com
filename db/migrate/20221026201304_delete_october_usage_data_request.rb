# frozen_string_literal: true

class DeleteOctoberUsageDataRequest < ActiveRecord::Migration[6.1]
  def up
    # no-op
  end

  def down
    # no-op
  end
end
