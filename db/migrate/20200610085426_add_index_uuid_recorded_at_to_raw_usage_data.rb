# frozen_string_literal: true

# See http://doc.gitlab.com/ce/development/migration_style_guide.html

class AddIndexUuidRecordedAtToRawUsageData < ActiveRecord::Migration[5.2]
  include Gitlab::Database::MigrationHelpers

  disable_ddl_transaction!

  def up
    add_concurrent_index :raw_usage_data, [:uuid, :recorded_at]
  end

  def down
    remove_concurrent_index :raw_usage_data, [:uuid, :recorded_at]
  end
end
