# frozen_string_literal: true

# See http://doc.gitlab.com/ce/development/migration_style_guide.html
# for more information on how to write migrations for GitLab.

class AddSearchUniqueVisitsToUsageData < ActiveRecord::Migration[5.2]
  def change
    add_column :usage_data, :search_unique_visits, :jsonb, default: {}
  end
end
