# frozen_string_literal: true

class AddCurrentVersionCheckToHosts < ActiveRecord::Migration[5.0]
  def change
    add_reference :hosts, :current_version_check, index: true, foreign_key: { on_delete: :cascade, to_table: :version_checks } # rubocop:disable Migration/AddReference
  end
end
