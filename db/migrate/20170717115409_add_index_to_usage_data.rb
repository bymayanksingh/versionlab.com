# frozen_string_literal: true

class AddIndexToUsageData < ActiveRecord::Migration[4.2]
  def change
    add_index :usage_data, :host_id # rubocop:disable Migration/AddIndex
  end
end
