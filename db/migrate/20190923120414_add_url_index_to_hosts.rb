# frozen_string_literal: true

class AddUrlIndexToHosts < ActiveRecord::Migration[5.0]
  include Gitlab::Database::MigrationHelpers

  disable_ddl_transaction!

  def up
    add_concurrent_index :hosts, :url
  end

  def down
    remove_index :hosts, :url if index_exists?(:hosts, :url) # rubocop:disable Migration/RemoveIndex
  end
end
