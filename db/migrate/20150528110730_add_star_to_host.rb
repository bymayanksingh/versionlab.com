# frozen_string_literal: true

class AddStarToHost < ActiveRecord::Migration[4.2]
  def change
    add_column :hosts, :star, :boolean, null: false, default: false
  end
end
