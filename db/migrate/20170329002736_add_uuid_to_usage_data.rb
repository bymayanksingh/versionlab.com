# frozen_string_literal: true

class AddUuidToUsageData < ActiveRecord::Migration[4.2]
  def change
    add_column :usage_data, :uuid, :string # rubocop:disable Migration/AddLimitToStringColumns
  end
end
