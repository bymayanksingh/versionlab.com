# frozen_string_literal: true

class AddMailSmtpServerToUsageData < ActiveRecord::Migration[5.2]
  def change
    add_column :usage_data, :mail_smtp_server, :string, limit: 255
  end
end
