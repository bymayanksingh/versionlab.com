# frozen_string_literal: true

# See http://doc.gitlab.com/ce/development/migration_style_guide.html
# for more information on how to write migrations for GitLab.

class LicenseTrialEndsOnToUsageData < ActiveRecord::Migration[5.1]
  def change
    add_column :usage_data, :license_trial_ends_on, :date
  end
end
